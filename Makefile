#!/usr/bin/env make
SHELL = /bin/bash

BUILD_DIR = ./build
BUILD_DIR_HTML = $(BUILD_DIR)/html
SRC_DIR = ./rules

SPH_SOURCE_DIR=./rules
PREPROC_DIR=$(SPH_SOURCE_DIR)/_scripts
SPH_STATIC_DIR=$(SPH_SOURCE_DIR)/_static
VENV_DIR=./venv


SPH_BUILD="$(VENV_DIR)/bin/sphinx-build"

.DEFAULT_GOAL := build_debug

.PHONY : all clean cleanall html preproc-spells


all : html

clean :
	rm -rf $(BUILD_DIR)
	make -C $(SRC_DIR) clean

distclean : clean

html :
	mkdir -p $(BUILD_DIR_HTML)
	make -C $(SRC_DIR) html WEB_ROOT:=$(abspath $(BUILD_DIR_HTML))

preproc-spells:
	@$(PYTHON) "$(PREPROC_DIR)/generate-spells.py" '$(SPH_STATIC_DIR)/objects/spells.ods'


#build_debug: preproc-spells
#	@-mkdir $(DEBUG_DIR)
#	$(SPH_BUILD) -b html $(SPH_SOURCE_DIR) $(DEBUG_DIR)
