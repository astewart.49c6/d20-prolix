#!/usr/bin/env python3
from argparse import ArgumentParser
import logging
from pathlib import Path
from pprint import pprint, pformat
import re

import jinja2 as j2
from markdown import markdown

from spells import load_spells

logger = logging.getLogger(__name__)
logger = logging.basicConfig(level=logging.DEBUG)

parser = ArgumentParser()
parser.add_argument('web_root', type=Path)
args = parser.parse_args()

PATH_SPELLS = Path('./spells')
PATH_TEMPLATES = Path('./_templates/html')

# load spells as a dictionary of python objects, sorted by name
spells = sorted(load_spells(PATH_SPELLS), key=lambda spell: spell.name)

j2_env = j2.Environment(loader=j2.FileSystemLoader(PATH_TEMPLATES))

def as_markdown(text: str):
    return markdown(text, output_format='html5')
j2_env.filters['as_markdown'] = as_markdown

def html_format_description(description: str):
    description = "<p>" + description + "</p>"
    description = re.sub(r'\n\n', '</p><p>', description)
    description = re.sub(r'[^>]\n', '<br/>', description)
    return description
j2_env.filters['html_format_description'] = html_format_description

template_spell = j2_env.get_template('div_spell.j2')
#template_spell_css = j2_env.get_template('style_spells.j2')


spell_divs = {}
for spell in spells:
    spell_alpha = spell.name[0].upper()
    sd_alpha = spell_divs.get(spell_alpha, [])
    sd = template_spell.render({'spell': spell.dict})
    try:
        spell_divs[spell_alpha].append(sd)
    except KeyError:
        spell_divs[spell_alpha] = [sd]


# write the spellbook html document
with open(args.web_root / 'spellbook.html', 'w') as fp:

    # generate the TOC links
    toc = []
    for spell in spells:
        toc

    j2_dict = {
        'spells': spells,
        'spell_divs': spell_divs,
        }
    template_spellbook = j2_env.get_template('page_spellbook.j2')
    fp.write(template_spellbook.render(j2_dict))
