#!/usr/bin/env python3
from argparse import ArgumentParser
import csv

from pyexcel_ods import get_data

parser = ArgumentParser()
parser.add_argument('ods_file', type=str)
parser.add_argument('sheet_names', type=str, nargs='*')
args = parser.parse_args()

print(f'INFO: Reading ods file: {args.ods_file}')
ods_data = get_data(args.ods_file)

from pprint import pprint

for sheet, data in ods_data.items():
    if len(args.sheet_names) == 0 or sheet in args.sheet_names:
        csv_name = f'{sheet}.csv'
        print(f'INFO: Writing sheet \"{sheet}\" to \"{csv_name}\"')
        with open(f'{sheet}.csv', 'w', newline='') as fp_csv:
            csv_writer = csv.writer(fp_csv, dialect='unix')
            for row in data:
                csv_writer.writerow(row)
