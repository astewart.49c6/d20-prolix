# Barbarian

*For some, there is only rage. In the ways of their people, in the fury of their passion, in the howl of battle, conflict is all these brutal souls know. Savages, hired muscle, masters of vicious martial techniques, they are not soldiers or professional warriors—they are the battle possessed, creatures of slaughter and spirits of war. Known as barbarians, these warmongers know little of training, preparation, or the rules of warfare; for them, only the moment exists, with the foes that stand before them and the knowledge that the next moment might hold their death. They possess a sixth sense in regard to danger and the endurance to weather all that might entail. These brutal warriors might rise from all walks of life, both civilized and savage, though whole societies embracing such philosophies roam the wild places of the world. Within barbarians storms the primal spirit of battle, and woe to those who face their rage.*

**Hit Die per Level:** d12

**Skill Points per Level:** 6 + WIS

**Class Skills:** Animal Handling; Perception; Survival

**Weapon Proficiencies:** Unarmed; Polearms; Blunt Weapons

**Non-Weapon Proficiencies:** Craft: Spear, Crude Weapons; Swim

**Wealth & Starting Equipment:** 3d6 x 10gp (avg. 105gp)

<html>
<iframe src="file:///mnt/sda1/usr0/projects/d20-prolix/build/html/_static/html/div_barbarian_class_table.html">
</iframe>
</html>

----

## Class Features

### Fast Movement

A barbarian's land speed is faster than the norm for his race by +10 feet. This benefit applies only when he is wearing no armor, light armor, or medium armor, and not carrying a heavy load. Apply this bonus before modifying the barbarian's speed because of any load carried or armor worn. This bonus stacks with any other bonuses to the barbarian's land speed.

### Rage

A barbarian can call upon inner reserves of strength and ferocity, granting him additional combat prowess. Starting at 1st level, a barbarian can rage for a number of rounds per day equal to 4 + his Constitution modifier. At each level after 1st, he can rage for 2 additional rounds. Temporary increases to Constitution, such as those gained from rage and spells like Bear’s Endurance, do not increase the total number of rounds that a barbarian can rage per day. A barbarian can enter rage as a free action. The total number of rounds of rage per day is renewed after resting for 8 hours, although these hours do not need to be consecutive.

While in rage, a barbarian gains a +4 morale bonus to his Strength and Constitution, as well as a +2 morale bonus on Will saves. In addition, he takes a –2 penalty to Armor Class. The increase to Constitution grants the barbarian 2 hit points per Hit Dice, but these disappear when the rage ends and are not lost first like temporary hit points. While in rage, a barbarian cannot perform delicate tasks or articulate elegant points. It is nearly impossible for them to resolve diplomatic situations in any form other than intimidation.

A barbarian can end his rage as a free action and is fatigued after rage for a number of rounds equal to 2 times the number of rounds spent in the rage. A barbarian cannot enter a new rage while fatigued or exhausted but can otherwise enter rage multiple times during a single encounter or combat. If a barbarian falls unconscious, his rage immediately ends, placing him in peril of death.

### Greater Rage

At 11th level, when a barbarian enters rage, the morale bonus to his Strength and Constitution increases to +6 and the morale bonus on his Will saves increases to +3.

Tireless Rage
=============

Starting at 17th level, a barbarian no longer becomes fatigued at the end of his rage.

Mighty Rage
===========

At 20th level, when a barbarian enters rage, the morale bonus to his Strength and Constitution increases to +8 and the morale bonus on his Will saves increases to +4.

Rage Powers
===========

As a barbarian gains levels, he learns to use his rage in new ways. Starting at 2nd level, a barbarian gains a rage power. He gains another rage power for every two levels of barbarian attained after 2nd level. A barbarian gains the benefits of rage powers only while raging, and some of these powers require the barbarian to take an action first. Unless otherwise noted, a barbarian cannot select an individual power more than once.

Any barbarian who meets the powers’ prerequisites can select and use rage powers. Totem rage powers grant powers related to a theme. A barbarian cannot select from more than one group of totem rage powers; for example, a barbarian who selects a beast totem rage power cannot later choose to gain any of the dragon totem rage powers (any rage power with “dragon totem” in its title).

Uncanny Dodge
=============

At 2nd level, a barbarian gains the ability to react to danger before his senses would normally allow him to do so. He cannot be surprised by combat, nor does he lose his Dex bonus to AC if the attacker is invisible. He still loses his Dexterity bonus to armor class if immobilized. A barbarian with this ability can still lose his Dexterity bonus to armor class if an opponent successfully uses the feint action against him.

If a barbarian already has uncanny dodge from a different class, he automatically gains improved uncanny dodge (see below) instead.

Improved Uncanny Dodge
======================

At 5th level and higher, a barbarian can no longer be flanked. This defense denies a rogue the ability to sneak attack the barbarian by flanking him, unless the attacker has at least four more rogue levels than the target has barbarian levels.

If a character already has uncanny dodge (see above) from another class, the levels from the classes that grant uncanny dodge stack to determine the minimum rogue level required to flank the character.

Trap Sense
==========
At 3rd level, a barbarian gains a +1 bonus on Reflex saves made to avoid traps and a +1 dodge bonus to armor class against attacks made by traps. These bonuses increase by +1 every three barbarian levels thereafter (6th, 9th, 12th, 15th, and 18th level). Trap sense bonuses gained from multiple classes stack.

Damage Reduction
================

At 7th level, a barbarian gains damage reduction. Subtract 1 from the damage the barbarian takes each time he is dealt damage from a weapon or a natural attack. At 10th level, and every three barbarian levels thereafter (13th, 16th, and 19th level), this damage reduction rises by 1 point. Damage reduction can reduce damage to 0 but not below 0.

Indomitable Will
================
While in rage, a barbarian of 14th level or higher gains a +4 bonus on Will saves to resist enchantment spells. This bonus stacks with all other modifiers, including the morale bonus on Will saves he also receives during her rage.
