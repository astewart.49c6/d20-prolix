=================
Actions in Combat
=================

In a normal round a character can make: one standard action and one move action, or a full-round action (in place of both). Additionally, a character can make any reasonable number of free actions, at the DM's discretion.


---
Standard Action
---

A standard action allows you to do something. The most common type of standard action is an attack - a single melee or ranged attack. Other common standard actions including casting a spell, concentrating to maintain an active spell, activating a magic item, and using a special ability.


---
Move Action
---

A move action allows you to move your speed or perform an action that takes a similar amount of time. You can move your speed, climb one-quarter of your speed, draw or stow a weapon or other item, stand up, pick up an object, or perform some equivalent action.


---
Special Actions
---

Two Weapon Fighting
===================

If you wield a second weapon in your off hand, you can get one extra attack per round with that weapon. Fighting in this way is very hard, however, and you suffer a –6 penalty with your regular attack or attacks with your primary hand and a –10 penalty to the attack with your off hand. You can reduce these penalties in two ways:
* If your off-hand weapon is light, the penalties are reduced by 2 each. (An unarmed strike is always considered light.)
* The Two-Weapon Fighting feat lessens the primary hand penalty by 2, and the off-hand penalty by 6. The table below summarizes the interaction of all these factors.

**Two Weapon Fighting Penalties**
==============|==============|=========
Circumstances | Primary Hand | Off Hand
--------------|--------------|---------
Normal penalties | -6 | -10
Off-hand weapon is light | -4 | -8
Two-Weapon Fighting feat | -4 | -4
Off-hand weapon is light and Two-Weapon Fighting feat | -2 | -2
==============|==============|=========

**Thrown Weapons**
The same rules apply when you throw a weapon from each hand. Treat a dart or shuriken as a light weapon when used in this manner, and treat a bolas, javelin, net, or sling as a one-handed weapon.
