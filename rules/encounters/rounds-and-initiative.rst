==========
Rounds and Initiative
==========

---
The Combat Round
---

Each round represents 6 seconds in the game world. At the table, a round presents an opportunity for each character involved in a combat situation to take an action. Anything a person could reasonably do in 6 seconds, your character can do in 1 round. Each round’s activity begins with the character with the highest initiative result and then proceeds, in order, from there. Each round of a combat uses the same initiative order. When a character’s turn comes up in the initiative sequence, that character performs his entire round’s worth of actions.


---
Initiative
---

At the start of a combat encounter, or during any encounter where it is important for character actions to follow a strategic order, the DM may place actors into an Initiative Order.

Normally, a characters position in the Initiative Order is determined by making an Initiative Check. Their initiative check expression is:

::
	Init = 1d20 + DEX + special_initiative_bonuses + contextual_modifiers


--------
Delaying and Skipping
--------

A player may choose to *delay* their turn in the Initiative Order to a later time.

To do so, a character must not have already taken an action in their turn. The player declares that they would like to delay to a particular location in the order (eg. "After the enemy", "before bob", "initiative count 15"). Then the character's new position in the Initiative Order is set to the delayed position.

It is obviously not possible to delay UP the initiative order, but a player assume the appearance of a higher initiative position by sacrificing their actions this round, delaying past the end of the round, and assuming a new position at a higher initiative count.

A player may also *skip* their turn - either because the player is not ready to play, or because their character is choosing to take no-action. Skipping is simply delaying a turn until the same initiative position in the next round.
