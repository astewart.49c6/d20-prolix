import yaml
import logging
from pprint import pprint, pformat
import os
import re

from spells import logger

RE_SPELL_FILE = re.compile(r'^spells_\w+\.(yml|yaml)')

class Spell():

    def __init__(self, id, **kwargs):
        self.id = id
        for key, value in kwargs.items():
            self.__setattr__(key, value)

    @property
    def components_string(self):
        if self.components is None or len(self.components) == 0:
            return 'None'
        else:
            return ', '.join(list(self.components))

    @property
    def dict(self):
        return {
            **self.__dict__,
            'components_string': self.components_string,
        }

    def __repr__(self):
        return pformat(self.__dict__)

def load_spells(root_dir):
    spells = []
    for root, dirs, fils in os.walk(root_dir):
        for fil in fils:
            if RE_SPELL_FILE.match(fil):
                logger.debug(f'Loading spell file: {fil}')
                with open(os.path.join(root, fil), 'r') as fp_source:
                    data = yaml.safe_load(fp_source)
                    for key, value in data.items():
                        spells.append(Spell(key, **value))
    return spells
